import { Injectable } from '@nestjs/common';
import { PDFDocument } from 'pdf-lib';
import { Buffer } from 'buffer';

@Injectable()
export class PdfService {
  async generatePdf(clip: any): Promise<Buffer> {
    const pdfDoc = await PDFDocument.create();
    pdfDoc.addPage();
    const pages = pdfDoc.getPages();
    // Add the title of the clip
    pages[0]?.drawText(clip.title, {
      x: 50,
      y: pages[0].getHeight() - 50,
      size: 24,
    });

    // Add the content of the clip
    pages[0]?.drawText(clip.content, {
      x: 50,
      y: pages[0].getHeight() - 100,
      size: 12,
    });

    // Add images of the clip if it has clip_media of the Image type
    if (clip.clipMedia && clip.clipMedia.length > 0) {
      const image = await pdfDoc.embedJpg(clip.clipMedia[0].url);
      pages[0].drawImage(image, {
        x: 50,
        y: pages[0].getHeight() - 150,
        width: 200,
        height: 200,
      });
    }

    // Save and cleanup the PDFDocument
    const pdfBytes = await pdfDoc.save();

    // Convert Uint8Array to Buffer
    return Buffer.from(pdfBytes);
  }
}