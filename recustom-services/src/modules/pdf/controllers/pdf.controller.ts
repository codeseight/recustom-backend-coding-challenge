import { Controller, Post, Body, StreamableFile } from '@nestjs/common';
import { PdfService } from '../services/pdf.service';
import { Clip } from '../../../graphql/generated';
import { Readable } from 'stream';

@Controller('pdf')
export class PdfController {
  constructor(private readonly pdfService: PdfService) {}

  @Post('generate')
    async generatePdf(@Body() clip: Clip): Promise<StreamableFile> {
    const pdfBytes = await this.pdfService.generatePdf(clip);
    const fileName = `clip-${clip.id}.pdf`;

    const readStream = new Readable();
    readStream.push(pdfBytes);
    readStream.push(null);
    
    return new StreamableFile(readStream, {
        type: 'application/pdf',
        disposition: `attachment; filename=${fileName}`,
    });
    }
}